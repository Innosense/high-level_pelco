# -*- coding: utf-8 -*-
"""
Created on Wed Dec 13 14:37:54 2023

@author: Yannick NOE
"""
import libpelco

class PTZ():
    
    def __init__(self, inAddress, inSerial):
        self.serial = inSerial
        self.pelco = libpelco.PELCO_Functions(inAddress)
        
        # Keyboard manual control, the list is used to update motion control only when a corresponding key is pressed
        self.LEFT_ARROW = 2424832
        self.RIGHT_ARROW = 2555904
        self.UP_ARROW = 2490368
        self.DOWN_ARROW = 2621440
    
        # Manual movement parameters, can be changed. Camera speed is 0-64 and zoom speed is 0-3.
        self.MANUAL_CAMERA_SPEED = 48
        self.MANUAL_ZOOM_SPEED = 1
    
        self.TILT_RANGE = (0,90) # Can be manually modified depending camera's parameter
    
    def manualInputControl(self, input_check): # Return valid motion and zoom speeds to be used in the setCameraSpeed function
        # Default motion values are set to 0. Use the "0" key to run this function while returning the default values.
        
        if input_check == self.LEFT_ARROW:
            self.serial.write(self.pelco.pantilt_left_pressed((self.MANUAL_CAMERA_SPEED,0)))
            
        elif input_check == self.RIGHT_ARROW:
            self.serial.write(self.pelco.pantilt_right_pressed((self.MANUAL_CAMERA_SPEED,0)))
            
        elif input_check == self.UP_ARROW:
            self.serial.write(self.pelco.pantilt_up_pressed((0,self.MANUAL_CAMERA_SPEED)))
            
        elif input_check == self.DOWN_ARROW:
            self.serial.write(self.pelco.pantilt_down_pressed((0,self.MANUAL_CAMERA_SPEED)))
            
        elif input_check == ord('-'):
            self.serial.write(self.pelco.pantilt_set_zoom_speed(self.MANUAL_ZOOM_SPEED))
            self.serial.write(self.pelco.pantilt_zoom_out())
            
        elif input_check == ord('+'):
            self.serial.write(self.pelco.pantilt_set_zoom_speed(self.MANUAL_ZOOM_SPEED))
            self.serial.write(self.pelco.pantilt_zoom_in())
            
        elif input_check == ord('0'):
            self.serial.write(self.pelco.pantilt_stop())
                    
    
    def setCameraSpeed(self, camera_in=(0,0), camera_zoom=0):
        # The pelco protocol requires specific binary values to be sent for each type of movement.
        # The direction of movement is independant from the input speed for pan and tilt
        # This function takes signed values as input and sends the proper binary data to the camera
        pan = camera_in[0]
        tilt = camera_in[1]
        camera_in = (abs(pan),abs(tilt))
        zoom = abs(camera_zoom)
        
        stop = (tilt==0 and pan==0 and camera_zoom==0)
        up = (tilt>0 and pan==0)
        down = (tilt<0 and pan==0)
        right = (tilt==0 and pan>0)
        left = (tilt==0 and pan<0)
        up_right = (tilt>0 and pan>0)
        up_left = (tilt>0 and pan<0)
        down_right = (tilt<0 and pan>0)
        down_left = (tilt<0 and pan<0)
        
        zoom_in = (camera_zoom>0)
        zoom_out = (camera_zoom<0)
        
        
        if stop:
            command = self.pelco.pantilt_stop()
        if up:
            command = self.pelco.pantilt_up_pressed(camera_in)
        if down:
            command = self.pelco.pantilt_down_pressed(camera_in)
        if left:
            command = self.pelco.pantilt_left_pressed(camera_in) 
        if right:
            command = self.pelco.pantilt_right_pressed(camera_in)
        if up_right:
            command = self.pelco.pantilt_up_right_pressed(camera_in)   
        if up_left:
            command = self.pelco.pantilt_up_left_pressed(camera_in)  
        if down_right:
            command = self.pelco.pantilt_down_right_pressed(camera_in)
        if down_left:
            command = self.pelco.pantilt_down_left_pressed(camera_in)
        if zoom_in:
            self.serial.write(self.pelco.pantilt_set_zoom_speed(zoom))
            command = self.pelco.pantilt_zoom_in()
        if zoom_out:
            self.serial.write(self.pelco.pantilt_set_zoom_speed(zoom))
            command = self.pelco.pantilt_zoom_out() 
            
        self.serial.write(command)
        
    def queryCameraPose(self):
        # The data response is two bytes of hex in hundreths of a degree
        self.serial.write(self.pelco.pantilt_query_pan())
        pan = self.serial.read(7) # Read the full serial response
        pan = int.from_bytes(pan[4:6], "big") # Extract the two bytes of data in hex and convert in int
        
        self.serial.write(self.pelco.pantilt_query_tilt())
        tilt = self.serial.read(7)
        tilt = int.from_bytes(tilt[4:6], "big")
        
        return (pan/100,tilt/100) # Converts to degrees
        
    def setCameraPose(self, pose):
        # Converts a value in degree to two bytes, the protocol expects a value in hundredths of a degree (0-35999)
        pan = int(pose[0]*100)%36000 # Converts degrees with up to two significant numbers into int of hundredths of degree, the modulo normalizes the input value
        pan = divmod(pan,0x100) # Converts into two bytes
        
        tilt = int(pose[1]*100)
        tilt = min(max(self.TILT_RANGE[0]*100,tilt),self.TILT_RANGE[1]*100) # This makes sure tilt input is between the defined range (0-90 by default)
        tilt = divmod(tilt,0x100)
    
        self.serial.write(self.pelco.pantilt_set_pan(pan))
        self.serial.write(self.pelco.pantilt_set_tilt(tilt))
        # print("Setting camera pose to:",pose)
## High-level class for pelcoD protocol application

## Description
This class was written to manage low-level frame generation for the pelcoD protocol used to control Pan Tilt Zoom (PTZ) cameras.

PelcoD documentation can be found at: https://www.bit-cctv.com/uploads/file/pelco-ptz-protocols-d-protocol-revision-5.0.1.pdf

## Usage
The PTZ_example file show the basic steps to use the class.

Serial is currently not implemened within de class so initial setup and close() routine has to be put at the beginning and end of code.
The camera is meant to be used with the openCV, this includes keyboard input management.

Current functions include:
 - Manual control of the camera: use the directional arrows for pan and tilt movement, + and - on the numpad to respectively zoom in and out and '0' on the numpad to stop movement. These shortcuts may be edited within the class.
 - Set camera speed: input pan speed, tilt speed and zoom speed in this format ((pan,tilt),zoom). Default parameters will stop the movement.
 - Query camera pose: returns currant pan angle and tilt angle as floats
 - Set camera pose: input desired pan and tilt angles as (pan,tilt), values outside of boundaries are normalized.

## Contributing
This class was written as a tool for a university project and only the necessary functions have been implemented.

Any contribution is welcome.

## Authors and acknowledgment
Original libpelco and commands_struct credited to : https://github.com/weqaar

Current iterations were rewritten for python 3 with added functionalities.

The libPTZ.py and PTZ_example were written by me.

## Project status
Development is stopped as the project requiring to work on this class is done and I don't currently have acces to a PTZ camera to further test it.

Maintainers welcome.

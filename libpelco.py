"""
Original work from https://github.com/weqaar

This library includes commonly used pelco functions.

Further documentation can be found at https://www.bit-cctv.com/uploads/file/pelco-ptz-protocols-d-protocol-revision-5.0.1.pdf
"""
from commands_struct_PELCO import *

class PELCO_Functions():

	def __init__(self,address= b'\x01'):
		global commands_struct 
		commands_struct = libpelco_structs()
		self.address = address

	# Returns: command
	# Input: Address (optional), command1 (optionnal), command2, data1 (often pan_speed), data2 (often tilt_speed)
	def _construct_cmd(self, _command2, _pan_speed, _tilt_speed, _command1 = b'\x00'):
		message = bytearray(7)
		message[0] = 255   #b'\xFF'
		message[1] = self.address
		message[2] = 0 # Always zero for the currently implemented functions
		message[3] = commands_struct._function_code[_command2]
		message[4] = _pan_speed
		message[5] = _tilt_speed
		message[6] = sum(message[1:6])%256 # Checksum

        # Printing frame, can be toggled on for debugging. Both decimal and hex value are printed
		if False:
			print("\nSynch byte is " + str(message[0]))
			print("Adress is " + str(message[1]))
			print("Command 1 is " + str(message[2]))
			print("Command 2 is " + str(message[3]))
			print("Data 1 is " + str(message[4]))
			print("Data 2 is " + str(message[5]))
			print("Checksum is " + str(message[6]) + "\n")
			print("Frame sent is:", message)
# 			for k in message:
# 				print(hex(k),end=" ")
# 			print()
	
		return message


	############ Commands #################################################################

	### STOP #############################################
    
	def pantilt_stop(self):
		retval = self._construct_cmd('STOP', 0, 0)
		return retval

	### UP ############################################### 

	def pantilt_up_pressed(self, _pan_tilt_speed):
		retval = self._construct_cmd('UP', int(_pan_tilt_speed[0]), int(_pan_tilt_speed[1]))
		return retval

	### UP-RIGHT #########################################

	def pantilt_up_right_pressed(self, _pan_tilt_speed):
		retval = self._construct_cmd('UP-RIGHT', int(_pan_tilt_speed[0]), int(_pan_tilt_speed[1]))
		return retval

	### UP-LEFT #########################################

	def pantilt_up_left_pressed(self, _pan_tilt_speed):
		retval = self._construct_cmd('UP-LEFT', int(_pan_tilt_speed[0]), int(_pan_tilt_speed[1]))
		return retval

	### DOWN #########################################

	def pantilt_down_pressed(self, _pan_tilt_speed):
		retval = self._construct_cmd('DOWN', int(_pan_tilt_speed[0]), int(_pan_tilt_speed[1]))
		return retval

	### DOWN-RIGHT #########################################

	def pantilt_down_right_pressed(self, _pan_tilt_speed):
		retval = self._construct_cmd('DOWN-RIGHT', int(_pan_tilt_speed[0]), int(_pan_tilt_speed[1]))
		return retval

	### DOWN-LEFT #########################################

	def pantilt_down_left_pressed(self, _pan_tilt_speed):
		retval = self._construct_cmd('DOWN-LEFT', int(_pan_tilt_speed[0]), int(_pan_tilt_speed[1]))
		return retval

	### LEFT #########################################

	def pantilt_left_pressed(self, _pan_tilt_speed):
		retval = self._construct_cmd('LEFT', int(_pan_tilt_speed[0]), int(_pan_tilt_speed[1]))
		return retval

	### RIGHT #########################################

	def pantilt_right_pressed(self, _pan_tilt_speed):
		retval = self._construct_cmd('RIGHT', int(_pan_tilt_speed[0]), int(_pan_tilt_speed[1]))
		return retval
    
	### RELEASE / STOP #########################################
    # A bit redundant, but useful to make code clearer
	def pantilt_released(self, _pan_tilt_speed):
		retval = self.pantilt_stop()
		return retval
    
    ### ZOOM #########################################
    
	def pantilt_set_zoom_speed(self,_zoom_speed):
		retval = self._construct_cmd('ZOOM-SPEED', 0, _zoom_speed)
		return retval
    
	def pantilt_zoom_in(self):
		retval = self._construct_cmd('ZOOM-IN', 0, 0)
		return retval

	def pantilt_zoom_out(self):
		retval = self._construct_cmd('ZOOM-OUT', 0, 0)
		return retval
    
    ### RETURN TO DEFAULT POSITION #########################################

	def pantilt_reset(self):
		retval = self._construct_cmd('RESET', 0, 0)
		return retval
    
    ### AUTOFOCUS #########################################

	def pantilt_set_autofocus(self, _autofocus): # 0 is on, 1 is off
		retval = self._construct_cmd('AUTOFOCUS', 0, _autofocus)
		return retval
    
    ### QUERY POSITION #########################################

	def pantilt_query_pan(self):
		retval = self._construct_cmd('QUERY-PAN', 0, 0)
		return retval

	def pantilt_query_tilt(self):
		retval = self._construct_cmd('QUERY-TILT', 0, 0)
		return retval
    
    ### SET POSITION #########################################

	def pantilt_set_pan(self, _pan_pose):
		retval = self._construct_cmd('SET-PAN', _pan_pose[0], _pan_pose[1])
		return retval

	def pantilt_set_tilt(self, _tilt_pose):
		retval = self._construct_cmd('SET-TILT', _tilt_pose[0], _tilt_pose[1])
		return retval

# Weqaar A. Janjua <weqaar.janjua@gmail.com>, 24 MAR 2014
# 
# Structures for Frame and Command Definitions for PELCO-D

class libpelco_structs():

	# Frame format:		|synch byte|address|command1|command2|data1|data2|checksum|
	# Bytes 2 - 6 are Payload Bytes
	_frame = {
		   'synch_byte':b'\xFF',		# Synch Byte, always FF		-	1 byte
		   'address':	b'\x00',		# Address			-	1 byte
		   'command1':	b'\x00',		# Command1			-	1 byte
		   'command2':	b'\x00', 	# Command2			-	1 byte
		   'data1':	b'\x00', 	# Data1	(PAN SPEED):		-	1 byte
		   'data2':	b'\x00', 	# Data2	(TILT SPEED):		- 	1 byte 
		   'checksum':	b'\x00'		# Checksum:			-       1 byte
		 }


	# Format: Command Hex Code
	_function_code = {
			  'DOWN':	16,#b'\x10',
			  'UP':		8,#b'\x08',	
			  'LEFT':	4,#b'\x04',
			  'RIGHT':	2,#b'\x02',
			  'UP-RIGHT':	10,#b'\x0A',
			  'DOWN-RIGHT':	18,#b'\x12',
			  'UP-LEFT':	12,#b'\x0C',
			  'DOWN-LEFT':	20,#b'\x14',
			  'STOP':	0,#b'\x00'
              'ZOOM-IN': 32,#b'\x20'
              'ZOOM-OUT': 64,#b'\x40'
              'ZOOM-SPEED': 37,#b'\x25'
              'RESET': 41,#b'\x29'
              'AUTOFOCUS': 49,#b'\x2B'
              'QUERY-PAN': 81,#b'\x51'
              'QUERY-TILT': 83,#b'\x53'
              'SET-PAN': 75,#b'\x4B'
              'SET-TILT': 77,#b'\x4D'
			}

# END 

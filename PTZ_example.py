# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 16:59:15 2024

@author: Yannick NOE
"""

import serial
import cv2
import libPTZ

if __name__ == '__main__':
    ########## Setup ##########
    
    # Setup openCV camera feed
    camera_feed = cv2.VideoCapture(0,cv2.CAP_DSHOW)
    
    # Setup serial communication
    serial_cam = serial.Serial('COM3')
    
    # Create camera object
    cam = libPTZ.PTZ(30, serial_cam)
    
    
    ########## Loop ##########
    while True:
        # Read camera feed
        ret, camera_img = camera_feed.read()
        
        # Display camera feed
        cv2.imshow('Camera', camera_img)
        
        # Checks keyboard input
        input_check = cv2.waitKeyEx(1)
        
        # If keyboard input is one of the directionnal arrows, move the camera, if it's '+' or '-', zoom in and out, '0' stops the movement
        cam.manualInputControl(input_check)
        
        # Send angle parameters to the camera
        if input_check == ord('t'):
            cam.setCameraPose((400,30))
            
        # Check the current camera's angles
        if input_check == ord('q'):
            print(cam.queryCameraPose())
            
        # Set pan and tilt speed
        if input_check == ord('s'):
            cam.setCameraSpeed((-15,-15))
        
        # Esc to quit
        if input_check == 27: 
            break
        
    
    ########## Closing routine ##########
    serial_cam.close()
    camera_feed.release()
    cv2.destroyAllWindows()